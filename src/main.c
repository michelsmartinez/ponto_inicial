#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <gtk/gtk.h>

bool executado = false;

typedef struct {
    GtkWidget *w_chk_ark;
    GtkWidget *w_chk_gparted;
    GtkWidget *w_chk_kmix;
    GtkWidget *w_chk_xsane;
    GtkWidget *w_chk_gwenview;
    GtkWidget *w_chk_evince;
    GtkWidget *w_chk_chk_transmission;
    GtkWidget *w_chk_gscreenshot;
    GtkWidget *w_chk_inkscape;
    GtkWidget *w_chk_nautilus;
    GtkWidget *w_chk_vlc;
    GtkWidget *w_chk_gnettool;
    GtkWidget *w_chk_firefox;
    GtkWidget *w_chk_gimp;
    GtkWidget *w_chk_elise;
    GtkWidget *w_chk_kate;
    GtkWidget *w_chk_nano;
} app_widgets;

void check_state(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script1[] = "xterm -e bash scripts/ark";
	system(script1);
    }
 }
 
 void check_state2(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script2[] = "xterm -e sudo bash scripts/kmix";
	system(script2);
    }
 }

 void check_state3(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script3[] = "xterm -e sudo bash scripts/xsane";
	system(script3);
    }
 }

 void check_state4(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script4[] = "xterm -e sudo bash scripts/gwenview";
	system(script4);
    }
 }
 
  void check_state5(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script5[] = "xterm -e sudo bash scripts/evince";
	system(script5);
    }
 }
 
   void check_state7(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script7[] = "xterm -e sudo bash scripts/transmission";
	system(script7);
    }
 }
 
  void check_state9(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script9[] = "xterm -e sudo bash scripts/gimp";
	system(script9);
    }
 }
 
  void check_state10(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script10[] = "xterm -e sudo bash scripts/inkscape";
	system(script10);
    }
 }
 
  void check_state11(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script11[] = "xterm -e sudo bash scripts/nautilus";
	system(script11);
    }
 }
 
  void check_state12(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script12[] = "xterm -e sudo bash scripts/vlc";
	system(script12);
    }
 }
 
  void check_state15(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script15[] = "xterm -e sudo bash scripts/gparted";
	system(script15);
    }
 }
 
  void check_state17(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script17[] = "xterm -e sudo bash scripts/gnettool";
	system(script17);
    }
 }
 
  void check_state20(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script20[] = "xterm -e sudo bash scripts/firefox";
	system(script20);
    }
 }
 
   void check_state21(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script21[] = "xterm -e sudo bash scripts/elise";
	system(script21);
    }
 }
 
 void check_state22(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script22[] = "xterm -e bash scripts/kate";
	system(script22);
    }
 }
 
 void check_state23(GtkWidget *widget, gpointer data){
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
        char script23[] = "xterm -e bash scripts/nano";
	system(script23);
    }
 }
 
int main(int argc, char *argv[]){

GtkBuilder *builder;
GtkWidget *janela, *vbox, *toggle;
app_widgets *widgets = g_slice_new(app_widgets);

gtk_init(&argc, &argv);


builder = gtk_builder_new();

// carregar tela de abertura


// tela principal
gtk_builder_add_from_file (builder, "glade/principal.glade", NULL);


janela = GTK_WIDGET(gtk_builder_get_object(builder, "principal"));
vbox = gtk_box_new(0,0);

// colocar sinais aqui
    toggle = gtk_toggle_button_new_with_mnemonic("chk_ark");
    toggle = gtk_check_button_new_with_mnemonic("chk_ark");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state), NULL);
        
    toggle = gtk_toggle_button_new_with_mnemonic("chk_kmix");
    toggle = gtk_check_button_new_with_mnemonic("chk_kmix");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state2), NULL);
        
    toggle = gtk_toggle_button_new_with_mnemonic("chk_xsane");
    toggle = gtk_check_button_new_with_mnemonic("chk_xsane");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
    g_signal_connect(toggle, "toggled", G_CALLBACK(check_state3), NULL);
    
    toggle = gtk_toggle_button_new_with_mnemonic("chk_gwenview");
    toggle = gtk_check_button_new_with_mnemonic("chk_gwenview");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state4), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_evince");
    toggle = gtk_check_button_new_with_mnemonic("chk_evince");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state5), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_transmission");
    toggle = gtk_check_button_new_with_mnemonic("chk_transmission");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state7), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_gimp");
    toggle = gtk_check_button_new_with_mnemonic("chk_gimp");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state9), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_inkscape");
    toggle = gtk_check_button_new_with_mnemonic("chk_inkscape");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state10), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_nautilus");
    toggle = gtk_check_button_new_with_mnemonic("chk_nautilus");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state11), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_vlc");
    toggle = gtk_check_button_new_with_mnemonic("chk_vlc");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state12), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_gparted");
    toggle = gtk_check_button_new_with_mnemonic("chk_gparted");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state15), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_gnettool");
    toggle = gtk_check_button_new_with_mnemonic("chk_gnettool");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state17), NULL);
        
            toggle = gtk_toggle_button_new_with_mnemonic("chk_firefox");
    toggle = gtk_check_button_new_with_mnemonic("chk_firefox");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state20), NULL);
        
    toggle = gtk_toggle_button_new_with_mnemonic("chk_elise");
    toggle = gtk_check_button_new_with_mnemonic("chk_elise");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state21), NULL);  
         
    toggle = gtk_toggle_button_new_with_mnemonic("chk_kate");
    toggle = gtk_check_button_new_with_mnemonic("chk_kate");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state22), NULL); 
        
    toggle = gtk_toggle_button_new_with_mnemonic("chk_nano");
    toggle = gtk_check_button_new_with_mnemonic("chk_nano");
    gtk_box_pack_start(GTK_BOX(vbox), toggle, 0,0,0);
        g_signal_connect(toggle, "toggled", G_CALLBACK(check_state23), NULL);  


gtk_builder_connect_signals(builder, widgets);

g_object_unref(builder);

gtk_widget_show(janela);
gtk_main();

// colocar codigo aqui
/*
// remove programa
char moveScript[] = "cp scripts/desinstalar /tmp";
char executaScript[] = "bash /tmp/desinstalar &";
system(moveScript);
system(executaScript);
*/
g_slice_free(app_widgets, widgets);

return 0;
}

void on_window_main_destroy()
{
    gtk_main_quit();
}
